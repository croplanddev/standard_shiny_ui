#' @export
actionButton <-function(inputId, label, icon=NULL, width=NULL, ...) {
  shiny::actionButton(inputId=inputId, label=label, icon=icon, width=width,
                      class="btn btn-primary action-button shiny-bound-input",...)
}

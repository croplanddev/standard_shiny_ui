#' Create a header for a dashboard page
#'
#' A dashboard header can be left blank, or it can include dropdown menu items
#' on the right side.
#'
#' @param title An optional title to show in the header bar.. By default, this
#'   will also be used as the title shown in the browser's title bar. If you
#'   want that to be different from the text in the dashboard header bar, set
#'   the \code{title} in \code{\link{dashboardPage}}.
#' @param titleWidth The width of the title area. This must either be a number
#'   which specifies the width in pixels, or a string that specifies the width
#'   in CSS units.
#' @param disable If \code{TRUE}, don't display the header bar.
#' @param ... Items to put in the header. Should be \code{\link{dropdownMenu}}s.
#' @param .list An optional list containing items to put in the header. Same as
#'   the \code{...} arguments, but in list format. This can be useful when
#'   working with programmatically generated items.
#'
#' @seealso \code{\link{dropdownMenu}}
#'
#' @examples
#' ## Only run this example in interactive R sessions
#' if (interactive()) {
#' library(shiny)
#'
#' # A dashboard header with 3 dropdown menus
#' header <- dashboardHeader(
#'   title = "Dashboard Demo",
#'
#'   # Dropdown menu for messages
#'   dropdownMenu(type = "messages", badgeStatus = "success",
#'     messageItem("Support Team",
#'       "This is the content of a message.",
#'       time = "5 mins"
#'     ),
#'     messageItem("Support Team",
#'       "This is the content of another message.",
#'       time = "2 hours"
#'     ),
#'     messageItem("New User",
#'       "Can I get some help?",
#'       time = "Today"
#'     )
#'   ),
#'
#'   # Dropdown menu for notifications
#'   dropdownMenu(type = "notifications", badgeStatus = "warning",
#'     notificationItem(icon = icon("users"), status = "info",
#'       "5 new members joined today"
#'     ),
#'     notificationItem(icon = icon("warning"), status = "danger",
#'       "Resource usage near limit."
#'     ),
#'     notificationItem(icon = icon("shopping-cart", lib = "glyphicon"),
#'       status = "success", "25 sales made"
#'     ),
#'     notificationItem(icon = icon("user", lib = "glyphicon"),
#'       status = "danger", "You changed your username"
#'     )
#'   ),
#'
#'   # Dropdown menu for tasks, with progress bar
#'   dropdownMenu(type = "tasks", badgeStatus = "danger",
#'     taskItem(value = 20, color = "aqua",
#'       "Refactor code"
#'     ),
#'     taskItem(value = 40, color = "green",
#'       "Design new layout"
#'     ),
#'     taskItem(value = 60, color = "yellow",
#'       "Another task"
#'     ),
#'     taskItem(value = 80, color = "red",
#'       "Write documentation"
#'     )
#'   )
#' )
#'
#' shinyApp(
#'   ui = dashboardPage(
#'     header,
#'     dashboardSidebar(),
#'     dashboardBody()
#'   ),
#'   server = function(input, output) { }
#' )
#' }
#' @export
dashboardHeader <- function(..., topLevelMenu=list(), title = NULL, titleWidth = NULL, disable = FALSE, .list = NULL) {
  items <- c(list(...), .list)
 #lapply(items, tagAssert, type = "li", class = "dropdown")

  titleWidth <- validateCssUnit(titleWidth)

  # Set up custom CSS for custom width.
  custom_css <- NULL
  if (!is.null(titleWidth)) {
    # This CSS is derived from the header-related instances of '230px' (the
    # default sidebar width) from inst/AdminLTE/AdminLTE.css. One change is that
    # instead making changes to the global settings, we've put them in a media
    # query (min-width: 768px), so that it won't override other media queries
    # (like max-width: 767px) that work for narrower screens.
    custom_css <- tags$head(tags$style(HTML(gsub("_WIDTH_", titleWidth, fixed = TRUE, '
      @media (min-width: 768px) {
        .main-header > .navbar {
          margin-left: _WIDTH_;
        }
        .main-header .logo {
          width: _WIDTH_;
        }
      }

    '))))
  }

  additional_css <- tags$head(tags$style(HTML( " 
    
    svg {
    width: 400px; 
    height: 48px;
    display:inline;
    position:absolute;
    top:0;
    right:0;
    }")))
  
  #main-header navbar navbar-expand-md

  tags$header(class = "main-header",
    custom_css,
    additional_css,
    style = if (disable) "display: none;",
    span(class = "logo", title),
    tags$nav(class = "navbar main-header navbar-expand-md", role = "navigation",
      # Embed hidden icon so that we get the font-awesome dependency
      span(shiny::icon("bars"), style = "display:none;"),
      # Sidebar toggle button
      a(href="#", class="sidebar-toggle", `data-toggle`="offcanvas",
        role="button",
        span(class="sr-only", "Toggle navigation")
      ),
          div(class="collapse navbar-collapse order-3", navbarMenu(topLevelMenu)),
            tags$svg(version="1.1",id="Layer_1",xmlns="http://www.w3.org/2000/svg", `xmlns:xlink`="http://www.w3.org/1999/xlink",x="0px",y="0px",viewBox="0 0 2464.07 311",style="enable-background:new 0 0 2464.07 311;", `xml:space`="preserve",HTML('
<style type="text/css">
	.st0{fill:white;}
</style>
<g>
	<g>
		<g>
			<path class="st0" d="M2305.25,88.62c0,0,5.64-39.22,39.22-39.44h39.05c0,0-7.71,39.44-39.73,39.44H2305.25z"/>
		</g>
		<g>
			<path class="st0" d="M2264.23,135.29c0,0,5.64-39.23,39.21-39.45h39.05c0,0-7.72,39.45-39.73,39.45H2264.23z"/>
		</g>
	</g>
	<g>
		<path class="st0" d="M1246.85,231.74c3.47,0,8.01,3.19,8.01,7.73c0,11.74-17.87,17.35-36.82,17.35c-40.02,0-61.1-36.81-61.1-72.57
			c0-35.76,23.21-72.58,63.5-72.58c18.14,0,35.22,6.94,35.22,14.94c0,6.13-3.21,10.67-10.67,10.67c-5.08,0-9.62-8-26.96-8
			c-27.21,0-40.29,30.68-40.29,53.35c0,29.88,15.21,56.57,40.29,56.57C1235.38,239.21,1241.52,231.74,1246.85,231.74"/>
	</g>
	<g>
		<path class="st0" d="M1304.48,244.54c0,8-3.46,12.28-9.61,12.28c-6.13,0-9.6-4.27-9.6-12.28v-120.6c0-8,3.47-12.28,9.6-12.28
			c6.15,0,9.61,4.27,9.61,12.28v13.34h0.54c6.93-15.74,19.21-25.61,31.21-25.61c9.07,0,12.81,5.07,12.81,10.41
			c0,5.34-3.74,8.27-13.08,10.41c-10.67,2.4-31.48,11.47-31.48,42.96V244.54z"/>
	</g>
	<g>
		<path class="st0" d="M1465.63,176.77c-0.8-23.75-14.14-49.09-40.01-49.09c-25.88,0-39.23,25.34-40.03,49.09H1465.63z
			 M1385.59,192.78c0.8,24.82,17.6,48.03,42.16,48.03c30.42,0,38.42-21.88,47.49-21.88c5.34,0,9.07,1.88,9.07,9.34
			c0,8.54-22.14,28.55-55.24,28.55c-45.35,0-64.3-36.81-64.3-72.57c0-35.76,21.08-72.58,61.1-72.58c40.03,0,60.57,33.62,60.57,69.38
			c0,5.07-1.6,11.74-13.08,11.74H1385.59z"/>
	</g>
	<g>
		<path class="st0" d="M1569.4,240.81c29.89,0,40.02-30.69,40.02-56.56c0-25.89-10.13-56.57-40.02-56.57
			c-29.08,0-40.29,30.69-40.29,56.57C1529.11,210.12,1539.52,240.81,1569.4,240.81 M1609.42,61.51c0-8.01,3.47-12.27,9.6-12.27
			c6.14,0,9.61,4.27,9.61,12.27v183.03c0,8-3.47,12.28-9.61,12.28c-6.13,0-9.6-4.27-9.6-12.28v-8.81h-0.79
			c-8.54,13.87-22.69,21.08-39.23,21.08c-40.81,0-61.09-32.55-61.09-72.57c0-35.76,19.48-72.58,60.56-72.58
			c16.54,0,28.82,6.94,39.76,21.07h0.79V61.51z"/>
	</g>
	<g>
		<path class="st0" d="M1683.35,244.54c0,8-3.47,12.28-9.61,12.28c-6.14,0-9.6-4.28-9.6-12.28V126.63c0-8,3.47-12.27,9.6-12.27
			c6.14,0,9.61,4.28,9.61,12.27V244.54z M1673.75,71.72c7.21,0,13.08,5.87,13.08,13.08c0,7.2-5.87,13.07-13.08,13.07
			c-7.2,0-13.07-5.87-13.07-13.07C1660.67,77.58,1666.55,71.72,1673.75,71.72"/>
	</g>
	<g>
		<path class="st0" d="M1745.26,244.54c0,8-3.47,12.28-9.62,12.28c-6.13,0-9.59-4.27-9.59-12.28V129.28h-12.54
			c-6.41,0-8.27-4.8-8.27-8.01c0-3.2,1.86-8,8.27-8h12.54V82.05c0-8.26,3.46-12.54,9.59-12.54c6.15,0,9.62,4.28,9.62,12.54v31.22
			h12.54c6.4,0,8.26,4.8,8.26,8c0,3.21-1.87,8.01-8.26,8.01h-12.54V244.54z"/>
	</g>
	<g>
		<path class="st0" d="M1837.74,170.34c-15.71-6.07-29.27-11.3-29.27-20.52c0-11.44,12.08-15.5,22.43-15.5
			c12.6,0,18.8,4.68,24.27,8.82c3.81,2.88,7.41,5.6,12.28,5.6c9.02,0,13.05-6.02,13.05-11.99c0-16.76-29.38-27.46-49.6-27.46
			c-24.57,0-50.67,14.87-50.67,42.41c0,23.08,23.56,32.86,44.33,41.51c16.16,6.72,31.43,13.07,31.43,24.12
			c0,8.29-9.38,16.84-25.1,16.84c-14.89,0-23.69-6.14-30.78-11.08c-4.82-3.35-8.98-6.26-13.78-6.26c-6.72,0-11.99,5.74-11.99,13.05
			c0,13.57,24.36,29.34,55.75,29.34c26.95,0,54.14-14.27,54.14-46.14C1884.24,188.31,1859.53,178.76,1837.74,170.34"/>
	</g>
	<g>
		<path class="st0" d="M1970.38,234.16c-27,0-36.57-26.89-36.57-49.91c0-23.02,9.58-49.93,36.57-49.93c27,0,36.57,26.9,36.57,49.93
			C2006.95,207.27,1997.38,234.16,1970.38,234.16 M2020.54,109.28c-6.62,0-13.33,4.77-13.33,13.86v2.46
			c-11.25-11.02-22.78-16.32-36.83-16.32c-42.56,0-64.81,37.72-64.81,74.97c0,37.24,22.25,74.95,64.81,74.95
			c14.45,0,26.97-6.4,36.83-17.97v4.12c0,9.09,6.7,13.86,13.33,13.86c6.46,0,13.33-4.86,13.33-13.86v-122.2
			C2033.87,114.15,2027,109.28,2020.54,109.28"/>
	</g>
	<g>
		<path class="st0" d="M2116.29,110.89h-12.3V78.85c0-5.14,1.89-6.96,7.22-6.96h5.88c13.21,0,15.19-7.84,15.19-12.52
			c0-7.73-8.17-12.52-15.72-12.52h-7.22c-20.41,0-33.59,12.98-33.59,33.07v30.97h-7.22c-8.85,0-14.12,4.68-14.12,12.52
			c0,7.84,5.27,12.52,14.12,12.52h7.22v108.33c0,9.07,5.53,14.94,14.12,14.94c8.58,0,14.12-5.87,14.12-14.94V135.93h12.3
			c8.84,0,14.11-4.69,14.11-12.52C2130.4,115.57,2125.13,110.89,2116.29,110.89"/>
	</g>
	<g>
		<path class="st0" d="M2199.95,134.32c22.3,0,34.06,19.26,34.97,39h-72.61C2163.2,161.39,2168.54,133.18,2199.95,134.32
			 M2249.66,214.94c-3.87,0-7.17,2.58-11.34,5.84c-7.22,5.64-17.11,13.38-36.68,13.38c-22.78,0-38.02-15.47-39.09-38.47h85.52
			c10.69,0,15.46-4.59,15.46-14.92c0-34.65-22.25-71.49-63.48-71.49c-43.43,0-66.15,37.72-66.15,74.97
			c0,34.57,17.67,74.95,67.48,74.95c29.38,0,59.22-18.31,59.22-31.46C2260.59,219.43,2254.96,214.94,2249.66,214.94"/>
	</g>
</g>
<g>
	<path class="st0" d="M142.85,175.09h58.12c1.69,0,3.06,1.37,3.06,3.06v55.47c0,0.82-0.31,1.61-0.89,2.19
		c-14.59,14.67-36.53,23.39-57.86,23.39c-42.06,0-77.77-34.45-77.77-75.23s35.71-75.02,77.77-75.02c20.65,0,41.87,8.18,56.45,22.01
		c1.28,1.21,1.2,3.28-0.12,4.46l-17.97,16.13c-1.19,1.07-3.02,1.06-4.16-0.07c-8.74-8.73-21.84-14.2-34.2-14.2
		c-24.73,0-45.65,21.56-45.65,46.7c0,25.36,20.92,46.91,45.65,46.91c9.4,0,18.99-3.03,26.98-8.37c0.85-0.57,1.34-1.54,1.34-2.56
		v-17.5c0-1.69-1.37-3.06-3.06-3.06h-27.69c-1.69,0-3.06-1.37-3.06-3.06v-18.18C139.79,176.46,141.16,175.09,142.85,175.09z"/>
	<path class="st0" d="M316.6,255.37l-31.35-44.53c-0.57-0.81-1.51-1.3-2.5-1.3h-18.38c-1.69,0-3.06,1.37-3.06,3.06v41
		c0,1.69-1.37,3.06-3.06,3.06H233.3c-1.69,0-3.06-1.37-3.06-3.06V114.54c0-1.69,1.37-3.06,3.06-3.06h62.24
		c31.49,0,54.74,20.29,54.74,49.03c0,20.26-11.58,36.42-29.47,44.07c-1.77,0.76-2.37,2.97-1.26,4.55l30.08,42.71
		c1.43,2.03-0.02,4.83-2.5,4.83h-28.01C318.11,256.67,317.17,256.18,316.6,255.37z M261.3,177.95c0,1.69,1.37,3.06,3.06,3.06h27.79
		c15.43,0,25.99-8.03,25.99-20.5c0-12.47-10.57-20.5-25.99-20.5h-27.79c-1.69,0-3.06,1.37-3.06,3.06V177.95z"/>
	<path class="st0" d="M468.67,233.84h-60.55c-1.25,0-2.37,0.75-2.84,1.91l-7.75,19.01c-0.47,1.15-1.59,1.91-2.84,1.91h-28.26
		c-2.18,0-3.66-2.2-2.84-4.22l56.68-139.06c0.47-1.15,1.59-1.91,2.84-1.91h30.75c1.24,0,2.37,0.75,2.84,1.91l56.68,139.06
		c0.82,2.01-0.66,4.22-2.84,4.22h-28.47c-1.25,0-2.37-0.75-2.84-1.91l-7.75-19.01C471.03,234.59,469.91,233.84,468.67,233.84z
		 M458.23,203.21l-17.01-41.85c-1.03-2.55-4.64-2.55-5.67,0l-17.01,41.85c-0.82,2.01,0.66,4.22,2.84,4.22h34.01
		C457.57,207.42,459.05,205.22,458.23,203.21z"/>
	<path class="st0" d="M556.79,203.52l-53.54-87.38c-1.25-2.04,0.22-4.66,2.61-4.66h29.1c1.1,0,2.12,0.59,2.66,1.55l32.65,57.47
		c1.16,2.05,4.1,2.07,5.3,0.05l34.04-57.56c0.55-0.93,1.55-1.5,2.64-1.5h27.67c2.39,0,3.86,2.62,2.61,4.66l-53.35,87.39
		c-0.29,0.48-0.45,1.03-0.45,1.6v48.49c0,1.69-1.37,3.06-3.06,3.06h-25.36c-1.69,0-3.06-1.37-3.06-3.06v-48.49
		C557.24,204.55,557.09,204,556.79,203.52z"/>
	<path class="st0" d="M661.84,114.54c0-1.69,1.37-3.06,3.06-3.06h48.93c48.82,0,83.48,30.01,83.48,72.49s-34.66,72.7-83.48,72.7
		h-48.93c-1.69,0-3.06-1.37-3.06-3.06V114.54z M717.85,228.35c27.68,0,47.34-18.39,47.34-44.38c0-25.78-19.65-44.17-47.34-44.17
		h-21.87c-1.69,0-3.06,1.37-3.06,3.06v82.42c0,1.69,1.37,3.06,3.06,3.06H717.85z"/>
	<path class="st0" d="M815.68,183.97c0-40.79,35.5-75.02,77.14-75.02c42.06,0,77.14,34.24,77.14,75.02s-35.08,75.23-77.14,75.23
		C851.19,259.2,815.68,224.76,815.68,183.97z M938.05,183.97c0-25.15-20.71-46.7-45.23-46.7c-24.3,0-45.01,21.56-45.01,46.7
		c0,25.36,20.71,46.91,45.01,46.91C917.34,230.88,938.05,209.32,938.05,183.97z"/>
	<path class="st0" d="M997.32,111.48h26.47c0.97,0,1.88,0.46,2.46,1.23l63.52,85.2c1.76,2.36,5.52,1.12,5.52-1.83v-81.54
		c0-1.69,1.37-3.06,3.06-3.06h25.15c1.69,0,3.06,1.37,3.06,3.06V253.6c0,1.69-1.37,3.06-3.06,3.06h-26.68
		c-0.97,0-1.88-0.46-2.46-1.23l-63.52-85.2c-1.76-2.36-5.52-1.12-5.52,1.83v81.54c0,1.69-1.37,3.06-3.06,3.06h-24.94
		c-1.69,0-3.06-1.37-3.06-3.06V114.54C994.25,112.85,995.62,111.48,997.32,111.48z"/>
</g>
')),
       div(class = "navbar-custom-menu",
        tags$ul(class = "nav navbar-nav",
          items
        )

      )
    )
  )
}


#' Create a dropdown menu to place in a dashboard header
#'
#' @param type The type of menu. Should be one of "messages", "notifications",
#'   "tasks".
#' @param badgeStatus The status of the badge which displays the number of items
#'   in the menu. This determines the badge's color. Valid statuses are listed
#'   in \link{validStatuses}. A value of \code{NULL} means to not display a
#'   badge.
#' @param ... Items to put in the menu. Typically, message menus should contain
#'   \code{\link{messageItem}}s, notification menus should contain
#'   \code{\link{notificationItem}}s, and task menus should contain
#'   \code{\link{taskItem}}s.
#' @param icon An icon to display in the header. By default, the icon is
#'   automatically selected depending on \code{type}, but it can be overriden
#'   with this argument.
#' @param headerText An optional text argument used for the header of the
#'   dropdown menu (this is only visible when the menu is expanded). If none is
#'   provided by the user, the default is "You have \code{x} messages," where
#'   \code{x} is the number of items in the menu (if the \code{type} is
#'   specified to be "notifications" or "tasks," the default text shows "You
#'   have \code{x} notifications" or  "You have \code{x} tasks," respectively).
#' @param .list An optional list containing items to put in the menu Same as the
#'   \code{...} arguments, but in list format. This can be useful when working
#'   with programmatically generated items.
#'
#' @seealso \code{\link{dashboardHeader}} for example usage.
#'
#' @export
dropdownMenu <- function(...,
  type = c("messages", "notifications", "tasks"),
  badgeStatus = "primary", icon = NULL, headerText = NULL,
  .list = NULL)
{
  type <- match.arg(type)
  if (!is.null(badgeStatus)) validateStatus(badgeStatus)
  items <- c(list(...), .list)

  # Make sure the items are li tags
  lapply(items, tagAssert, type = "li")

  dropdownClass <- paste0("dropdown ", type, "-menu")

  if (is.null(icon)) {
    icon <- switch(type,
      messages = shiny::icon("envelope"),
      notifications = shiny::icon("warning"),
      tasks = shiny::icon("tasks")
    )
  }

  numItems <- length(items)
  if (is.null(badgeStatus)) {
    badge <- NULL
  } else {
    badge <- span(class = paste0("label label-", badgeStatus), numItems)
  }

  if (is.null(headerText)) {
    headerText <- paste("You have", numItems, type)
  }

  tags$li(class = dropdownClass,
    a(href = "#", class = "dropdown-toggle", `data-toggle` = "dropdown",
      icon,
      badge
    ),
    tags$ul(class = "dropdown-menu",
      tags$li(class = "header", headerText),
      tags$li(
        tags$ul(class = "menu",
          items
        )
      )
      # TODO: This would need to be added to the outer ul
      # tags$li(class = "footer", a(href="#", "View all"))
    )
  )

}



#' Create a message item to place in a dropdown message menu
#'
#' @param from Who the message is from.
#' @param message Text of the message.
#' @param icon An icon tag, created by \code{\link[shiny]{icon}}.
#' @param time String representing the time the message was sent. Any string may
#'   be used. For example, it could be a relative date/time like "5 minutes",
#'   "today", or "12:30pm yesterday", or an absolute time, like "2014-12-01 13:45".
#'   If NULL, no time will be displayed.
#' @param href An optional URL to link to.
#'
#' @family menu items
#' @seealso \code{\link{dashboardHeader}} for example usage.
#' @export
messageItem <- function(from, message, icon = shiny::icon("user"), time = NULL,
  href = NULL)
{
  tagAssert(icon, type = "i")
  if (is.null(href)) href <- "#"

  tags$li(
    a(href = href,
      icon,
      h4(
        from,
        if (!is.null(time)) tags$small(shiny::icon("clock-o"), time)
      ),
      p(message)
    )
  )
}


#' Create a notification item to place in a dropdown notification menu
#'
#' @param text The notification text.
#' @param icon An icon tag, created by \code{\link[shiny]{icon}}.
#' @param status The status of the item This determines the item's background
#'   color. Valid statuses are listed in \link{validStatuses}.
#' @param href An optional URL to link to.
#'
#' @family menu items
#' @seealso \code{\link{dashboardHeader}} for example usage.
#' @export
notificationItem <- function(text, icon = shiny::icon("warning"),
  status = "success", href = NULL)
{
  tagAssert(icon, type = "i")
  validateStatus(status)
  if (is.null(href)) href <- "#"

  # Add the status as another HTML class to the icon
  icon <- tagAppendAttributes(icon, class = paste0("text-", status))

  tags$li(
    a(href = href, icon, text)
  )
}


#' Create a task item to place in a dropdown task menu
#'
#' @param text The task text.
#' @param value A percent value to use for the bar.
#' @param color A color for the bar. Valid colors are listed in
#'   \link{validColors}.
#' @param href An optional URL to link to.
#'
#' @family menu items
#' @seealso \code{\link{dashboardHeader}} for example usage.
#' @export
taskItem <- function(text, value = 0, color = "aqua", href = NULL) {
  validateColor(color)
  if (is.null(href)) href <- "#"

  tags$li(
    a(href = href,
      h3(text,
        tags$small(class = "pull-right", paste0(value, "%"))
      ),
      div(class = "progress xs",
        div(
          class = paste0("progress-bar progress-bar-", color),
          style = paste0("width: ", value, "%"),
          role = "progressbar",
          `aria-valuenow` = value,
          `aria-valuemin` = "0",
          `aria-valuemax` = "100",
          span(class = "sr-only", paste0(value, "% complete"))
        )
      )
    )
  )
}

#' Simple function to construct a font-awesome icon string
#' @export
iconize <- function(string) {
  paste0("<i class='",paste0("fa fa-", string), "'></i>")
}

#' Function to construct a navbar menu based on a list
#' @param items list with the following structure: list(list(href="target of menu", icon="font-awesome icon class to go with link, optional", text="Menu item text", id="link id, optional"), list(text="dropdown menu", dropdown=list(list(href="target of submenu", icon="font-awesome icon class to go with link, optional", text="Menu item text", id="link id, optional"))))
#' @export
navbarMenu <- function(items) {

  items <-lapply(items, FUN=function(x) {
   if(all(c("href","icon", "text") %in% names(x))) {
     if("id" %in% names(x)) {
       tags$li(class="nav-item", HTML(paste0("<a href='", x$href, "' class='nav-link' id='", x$id,"'>"), iconize(x$icon), x$text,HTML("</a>")))
       
     } else {
       tags$li(class="nav-item", HTML(paste0("<a href='", x$href, "' class='nav-link'>"), iconize(x$icon), x$text,HTML("</a>")))
       
     }
   } else if(all(c("icon", "text", "dropdown") %in% names(x))) {
     submenu <-lapply(x$dropdown, FUN=function(z) {
       if("id" %in% names(z)) {
         tags$li( HTML(paste0("<a href='", z$href, "' class='dropdown-item' id='",z$id,"'>"), iconize(z$icon), z$text,HTML("</a>")))
         
       } else {
         tags$li( HTML(paste0("<a href='", z$href, "' class='dropdown-item'>"), iconize(z$icon), z$text,HTML("</a>")))
         
       }
      })

     tags$li(class="nav-item dropdown", HTML(paste0("<a href='#' class='nav-link dropdown-toggle' data-toggle='dropdown' >", iconize(x$icon), x$text, "</a>")),
            tags$ul(class="dropdown-menu border-0 shadow", tagList(submenu))

             )

   } else {
   stop("fail")
   }


  })
  
  

  result <- #tags$div(class="navbar",
      tags$ul(class="navbar-nav",
         tagList(items)
         #)
      )
  return(result)
}

#' @description LABEL_WRAP breaks lines at the specified point in order to create nicer looking plot labels. This piece of code is actually a copy of stringr::str_wrap
#' @title label_wrap
#' @param string string to wrap
#' @param width maximum line width, in number of characters
#' @param indent indentation to provide before the start of other content
#' @param exdent padding to provide after other content
#' @param collapse String to use as glue between separated strings. Defaults to unix-style newline
#' @importFrom stringi stri_wrap stri_c
#' @export
label_wrap <- function (string, width = 35, indent = 0, exdent = 0, collapse="\n")
{
  if (width <= 0)
    width <- 1
  out <- stringi::stri_wrap(string, width = width, indent = indent,
                            exdent = exdent, simplify = FALSE)
  vapply(out, stringi::stri_c, collapse = collapse, character(1))
}
